/***********************
* Adobe Edge Animate Composition Actions
*
* Edit this file with caution, being careful to preserve 
* function signatures and comments starting with 'Edge' to maintain the 
* ability to interact with these actions from within Adobe Edge Animate
*
***********************/
(function($, Edge, compId){
var Composition = Edge.Composition, Symbol = Edge.Symbol; // aliases for commonly used Edge classes

   //Edge symbol: 'stage'
   (function(symbolName) {
      
      
      Symbol.bindElementAction(compId, symbolName, "document", "compositionReady", function(sym, e) {
yepnope(
	{nope: 
		[
		"css/jquery-ui.css",
		"css/idangerous.swiper.css",
		"css/keyboard.css",
		"css/jquery.mobile-1.3.2.css",
		"css/style.css",
		"edge_includes/jquery-ui.min.js",
		"edge_includes/idangerous.swiper.js",
		"edge_includes/EdgeCommons.min.js", 
		"edge_includes/kinetic-v4.5.4.min.js",
		"edge_includes/jquery.keyboard.min.js",
		"edge_includes/jquery.mobile-1.3.2.js"
		], 
		complete: init
	}
);

$(document).bind('mobileinit',function(){
   $.mobile.selectmenu.prototype.options.nativeMenu = false;
});


var stage = sym.getComposition().getStage();
var lotStage = stage.getSymbol('lotMap');

window.dataPath = '/home/sds/playing/';


stage.setVariable("community", "mahoganyAT");
stage.setVariable("currentOptions", {});
stage.setVariable("currentModels", []);
stage.setVariable("currentElevation", "");
stage.setVariable("currentPhase", "19");

// ** checks to see if object/array is empty
function isEmptyObject(obj) {
   var name;
   for (name in obj) {
      return false;
   }
   return true;
}

// ** Community screen video setup ** //
var viewVid = stage.$('community').find("div[id$='videoPlayer']");
viewVid.hide();

if (window.process !== undefined) {
   viewVid.html('<video id="videoplay" width="1920" height="1080" poster=""><source src="' + window.dataPath + 'mahogany.mp4" type="video/mp4" /></video>');
} else {
   viewVid.html('<video id="videoplay" width="1920" height="1080" poster=""><source src="videos/mahogany.mp4" type="video/mp4" /></video>');
};

// ** Selection screen setup ** //
function selectionReady(err, data) {
   if (data !== 'success') {
      data = JSON.parse(data);
   } else {
      data = err;
   }

   var wrapper = stage.$("modelSelection").find("div[id$='wrapper']");

	stage.setVariable("rawData", data);

   modelListArray = [];

   for (var i = 0; i < data.length; i++) {
   	var modelData = data[i];

   	var modelSymbol = stage.createChildSymbol("modelBox", "modelSelection");
      modelListArray.push(modelSymbol);

      modelSymbol.setVariable("modelData",modelData);
   	var modelElement = modelSymbol.getSymbolElement();

   	$(modelElement).children().find("div[id$='title']").text(modelData.name);
   	$(modelElement).children().find("div[id$='sqFt']").text(modelData.area + " Sq.Ft");
   	$(modelElement).children().find("div[id$='extra']").html(modelData.highlights.join("<br/>"));

   	var modelName =  modelData.name.replace(/ /g,'');
   	var imageUrl = "images/" + modelName.charAt(0).toLowerCase() + modelName.slice(1) + "/select.jpg";

   	$(modelElement).children().find("div[id$='image']").css({"background-image":"url(" + imageUrl + ")"});
		$(modelElement).addClass("swiper-slide");

   	$(modelElement).detach().appendTo(wrapper);
   }

   $.each(modelListArray, function(count, symbolInArray){
      //var menuElement = $(symbolInArray.getSymbolElement());
      var menuElement = symbolInArray.$('titleRect');

      function modelClick() {
         stage.setVariable("currentScreen", "brochure");
         var symbolData = symbolInArray.getVariable("modelData");
         stage.setVariable("currentModel", symbolData);
         stage.play("fade-out-selection");
      }
      menuElement.bind("click", modelClick);
   });

	var mySwiper = new Swiper('.swiper-container',{
		loop: false,
		freeMode: true,
      freeModeFluid: true,
      slidesPerView: 2.78,
      resizeReInit: true
	});  

   stage.play("fade-in-selection");  
}


// ** Brochure screen setup ** //
function loadBrochure(callback) {
	var data = stage.getVariable("currentModel");
	var brochure = stage.$("brochure");

	$(brochure).find("div[id$='modelName']").text(data.name);
	$(brochure).find("div[id$='sqFt']").text(data.area + " Sq.Ft.");

	var modelName =  data.name.replace(/ /g,'');
   modelName = modelName.charAt(0).toLowerCase() + modelName.slice(1);
   var floorplanContainer = $(brochure).find("div[id$='floorplans']");

   var floorPlanUrl = "images/" + modelName + "/floorplan.png";
   $(floorplanContainer).css({"background-image":"url(" + floorPlanUrl + ")"});
   $(floorplanContainer).html("");

   // generate and display model's elevations
   var elevationUrl = [];
   var elevationSymbols = [];
   var phaseOnly = "";

   for (var i in data.elevations) {
      var opt = data.elevations[i];
      elevationUrl.push([opt,"images/" + modelName + "/" + opt.replace(/[^A-Z0-9]+/ig, "").toLowerCase() + ".jpg"]);
   };

   console.log(elevationUrl);

   if (elevationUrl[0][0] === "Coastal" | elevationUrl[0][0] === "Shingle") {
   	phaseOnly = " (Phase 45 Only)";
   } else {
   	phaseOnly = "";
   }
   $(brochure).find("div[id$='elevationImage']").css({"background-image":"url(" + elevationUrl[0][1] + ")"});
   $(brochure).find("div[id$='elevationName']").text(elevationUrl[0][0] + " Elevation" + phaseOnly);
   stage.setVariable("currentElevation", elevationUrl[0][0]);
   for (var number in elevationUrl) {
      var elevationElement = $(brochure).find("div[id$='elevation" + number + "']");
      elevationElement.css({"background-image":"url(" + elevationUrl[number][1] + ")"});
      elevationSymbols.push(elevationElement);
   };

   $.each(elevationSymbols, function(count,eleSymbol){
      eleSymbol.bind("click",function(){
      	 if (elevationUrl[count][0] === "Coastal" | elevationUrl[count][0] === "Shingle") {
      	 	phaseOnly = " (Phase 45 Only)";
      	 } else {
      	 	phaseOnly = "";
      	 };
         $(brochure).find("div[id$='elevationImage']").css({"background-image":"url(" + elevationUrl[count][1] + ")"});
         $(brochure).find("div[id$='elevationName']").text(elevationUrl[count][0] + " Elevation" + phaseOnly);
         stage.setVariable("currentElevation", elevationUrl[count][0]);
      });
   });

   // generate and display model's floorplan option list
	var optionsList = $(brochure).find("ul[id='options-list']");

	if (optionsList.length !== 0) {
		$(optionsList).html("");
	} else {
		$(brochure).find("div[id$='optionsContainer']").append("<ul id='options-list'></ul>");
		optionsList = $(brochure).find("ul[id='options-list']");
	}

   // check if model has gallery
   if (isEmptyObject(data.gallery)) {
      $(brochure).find("div[id$='galleryLink']").css({"display":"none"})
   } else {
      $(brochure).find("div[id$='galleryLink']").css({"display":"block"})
   }

   var galleryPath = "images/" + modelName + "/gallery/";
   $.each(data.gallery, function (number, image) {
      $(brochure).find("div[id$='gallery" + number + "']").css({"background-image":"url(" + galleryPath + image.name + "_thumb.jpg)"});
      $(brochure).find("div[id$='gallery" + number + "']").bind("click",function() {
         var galleryConfig = {
            width: image.width,
            height: image.height,
            type: "image",
            source: galleryPath + image.name + ".jpg"
         };                 
         EC.Spotlight.open(galleryConfig);                  
      });
   });

   // function to clear brochure info (upon exit)
   function clearBrochure(callback) {
      $(brochure).find("div[id$='modelName']").text("");
      $(brochure).find("div[id$='sqFt']").text("??? Sq.Ft.");
      $(brochure).find("div[id$='elevationImage']").css({"background-image":"url()"});
      $(brochure).find("div[id$='elevationName']").text("??? Elevation");
      for (var i=0; i<6; i++) {
         $(brochure).find("div[id$='elevation" + i + "']").css({"background-image":"url()"});
         $(brochure).find("div[id$='elevation" + i + "']").unbind("click");
      }
      for (var i=1; i<=16; i++) {
         $(brochure).find("div[id$='gallery" + i + "']").css({"background-image":"url()"});
         $(brochure).find("div[id$='gallery" + i + "']").unbind("click");
      }
   }
   window.clearBrochure = clearBrochure;

   // show/hide model options
	function showHideOption(option) {
		var optionImage = $(floorplanContainer).find("img[id=" + option + "]");
		var optionLI = $(optionsList).find("li[id=" + option + "]");
		var currentOptions = stage.getVariable("currentOptions");

		if ($(optionImage).is(':visible')) {
			$(optionImage).hide();
			$(optionLI).css({"background-color":"transparent"});
			delete currentOptions[option];
		} else {
			$(optionImage).show();
			$(optionLI).css({"background-color":"#b5121b"});
			var currentOpt = data.options[option];
			currentOptions[option] = currentOpt;

			for (var number in data.options) {
				var opt = data.options[number];

				if ((number.toString() !== option.toString()) && ((opt.floor.indexOf(currentOpt.floor) !== -1) || (currentOpt.floor.indexOf(opt.floor) !== -1))) {
					$(floorplanContainer).find("img[id=" + number + "]").hide();
					$(optionsList).find("li[id=" + number + "]").css({"background-color":"transparent"});
					delete currentOptions[number];
				}

			}	
		}
		stage.setVariable("currentOptions", currentOptions);
	}
	window.showHideOption = showHideOption;


	function showHideModel(model) {
		var modelName = model.textContent;
		var modelLink = $("div[id=" + model.id + "]");
		console.log(modelLink);
		var currentModels = stage.getVariable("currentModels");

		if (currentModels.indexOf(modelName) !== -1) {
			$(modelLink).css({"background-color":"transparent"});
			currentModels.splice(currentModels.indexOf(modelName), 1);
		} else {
			$(modelLink).css({"background-color":"#b5121b"});
			currentModels.push(modelName);
		}
		stage.setVariable("currentModels", currentModels);
	}
	window.showHideModel = showHideModel;


	var optionTotal = 0;
	for (var option in data.options) {
		optionTotal += 1;
	}

	for (var option in data.options) {
		var optionUrl = "images/" + modelName + "/option" + option + ".png";

		var optionListStyling = "";
		if (optionTotal > 11) {
			optionListStyling = "font-size: 25px; margin: 5px 0px; padding: 8px 0px;";
		} else if (optionTotal > 7) {
			optionListStyling = "font-size: 30px; margin: 5px 0px; padding: 10px 0px;";
		}

		$(optionsList).append("<li id='" + option + "'  onclick='window.showHideOption(" + option + ")' style='" + optionListStyling + "'>" + data.options[option].optionName + "</li>");
		$(floorplanContainer).append("<img id='" + option + "' style='position: absolute; display: none' src=" + optionUrl + ">");
	}

	callback();
}
window.loadBrochure = loadBrochure;

function sendEmail() {

	/*

	first = firstName;
	last = lastName;
	email = email;
	community = community;
	brochures = brochuresString;
	options = optionString;

	*/

	var firstName = $("input[id=first]").val();
	var lastName = $("input[id=last]").val();
	var email = $("input[id=email]").val();
	var community = stage.getVariable("community");
	var brochures = stage.getVariable("currentModels");
	var options = stage.getVariable("currentOptions");
	var elevation = stage.getVariable("currentElevation");

	var optionString = [];

	for (var option in options) {
		optionString.push(option + ": " + options[option].optionName);
	}

	var postData = {
		first: firstName,
		last: lastName,
		email: email,
		community: community,
		brochures: brochures.join(','),
		options: optionString.join(','),
		elevation: elevation
	}

	console.log(postData);

	function success () {
		console.log('Sent email!');
	}

   if (firstName !== '' && lastName !== '' && email !== '') {
   	$.ajax({
   	  type: "POST",
   	  url: "http://stepperhomes.anvy.net/send-brochures",
   	  data: postData,
   	  success: success
   	});
   }

	stage.setVariable("currentModels", []);
	stage.getSymbol("brochure").play("fade-out-email");
}
window.sendEmail = sendEmail;

// ** Lot map symbol ** //

// Set defaults
stage.setVariable("zoomed", 30);
stage.setVariable("currentZoom", 30);
stage.setVariable("zooming", false);
stage.setVariable("password", "1956");
stage.setVariable("currentPassword", "");
stage.setVariable("keys", false);
stage.setVariable("auth", false);

// Set constants
var STATUSES = [
	{ status: 'Available', 
	  colour: 'green'
	},
	{ status: 'On Hold',
	  colour: 'yellow'
	},
	{ status: 'SPEC',
	  colour: 'blue'
	},
	{ status: 'Sold',
	  colour: 'red'
	},
	{ status: 'Show Home',
	  colour: 'purple' 
	},
	{ status: 'Pool',
	  colour: 'cyan' 
	}
]


var MODELS = ['Alder Bay', 'Ash River', 'Aspen Falls', 'Birch Falls', 'Cedar Spring', 'Cypress Falls', 'Hemlock Cove', 'Maple Creek', 'Oak Bay', 'Pine Harbour', 'Redwood Falls', 'Willow Creek']

var ELEVATIONS = [
	// { elevation: '',
	//   name: ''
	// },
	{ elevation: 'artscrafts',
	  name: 'Arts & Crafts'
	},
	{ elevation: 'craftsman',
	  name: 'Craftsman'
	},
	{ elevation: 'frenchcountry',
	  name: 'French Country'
	},
	{ elevation: 'prairie',
	  name: 'Prairie'
	},
	{ elevation: 'coastal',
	  name: 'Coastal'
	},
	{ elevation: 'shingle',
	  name: 'Shingle'
	}
]

// Global functions that are either used in this file
// or are used in child symbols. The ones with broader
// scope are stored in 'window'

// Creates the form for editing lot map data
function create_auth_form(data) {
	var auth = stage.getVariable('auth');

	if (auth) {
		console.log(data);
		var adminBox = lotStage.getSymbol('auth').$('adminBox');
		if (data !== undefined) {
			// Build the different options selects dynamically
			// Status
			// var statusSelect = "<select name='status' id='status' data-native-menu='false' data-theme='a' onChange='window.lotUpdate(this)'>";
			// var statusOptions = "";
			
			var statusSelect = "<select name='status' id='status' data-native-menu='false' data-theme='a' onChange='window.lotUpdate(this)'>";
			var statusOptions = "<option data-placeholder='true' value=''>Select Lot Status</option>";

			for (var i = 0; i < STATUSES.length; i++) {
				var status = STATUSES[i];
				var option = "";

				if (data.status === status.status) {
					option = "<option value='" + status.status + "' selected>" + status.status + "</option>";
				} else {
					option = "<option value='" + status.status + "'>" + status.status + "</option>";
				}
				statusOptions += option;
			}
			statusSelect += statusOptions + "</select>";

			// Models
			// var modelSelect = "<select id='model' data-native-menu='false' data-theme='a' onChange='window.lotUpdate(this)'>";
			// var modelOptions = "";

			var modelSelect = "<select id='model' data-native-menu='false' data-theme='a' onChange='window.lotUpdate(this)'>";
			var modelOptions = "<option data-placeholder='true' value=''>Select Model</option>";

			if (data.model === '') {
				modelOptions += "<option value='' data-placeholder='false' selected>(No Model)</option>";
			} else {
				modelOptions += "<option value='' data-placeholder='false'>(No Model)</option>";
			}

			for (var i = 0; i < MODELS.length; i++) {
				var model = MODELS[i];
				var option = "";

				if (data.model === model) {
					option = "<option value='" + model + "' selected>" + model + "</option>";
				} else {
					option = "<option value='" + model + "'>" + model + "</option>";
				}
				modelOptions += option;
			}
			modelSelect += modelOptions + "</select>";

			// Elevations
			// var elevationSelect = "<select id='elevation' data-native-menu='false' data-theme='a' onChange='window.lotUpdate(this)'>";
			// var elevationOptions = "";

			var elevationSelect = "<select id='elevation' data-native-menu='false' data-theme='a' onChange='window.lotUpdate(this)'>";
			var elevationOptions = "<option data-placeholder='true' value=''>Select Elevation</option>";

				if (data.elevation === '') {
				elevationOptions += "<option value='' data-placeholder='false' selected>(No Elevation)</option>";
			} else {
				elevationOptions += "<option value='' data-placeholder='false'>(No Elevation)</option>";
			}
			
			for (var i = 0; i < ELEVATIONS.length; i++) {
				var elevation = ELEVATIONS[i];
				var option = "";

				if (data.elevation === elevation.name) {
					option = "<option value='" + elevation.name + "' selected>" + elevation.name + "</option>";
				} else {
					option = "<option value='" + elevation.name + "'>" + elevation.name + "</option>";
				}
				elevationOptions += option;
			}
			elevationSelect += elevationOptions + "</select>";

			var block = "??";
			if (data.block !== undefined) {
				block = data.block;
			}

			adminBox.html("<div>" +
				"<span id='lot-title'>" + data.lot + "</span><br>" + 
				"<form id='update-form' method='post' action='.'>" +
				"<input name='block' type='hidden' value='" + data.block + "'>" +
				"<input name='lot' type='hidden' value='" + data.lot + "'>" +
				statusSelect +
				modelSelect +
				elevationSelect +
				"</form>" +
			"</div>"); 

         $('#status').selectmenu();
         $('#status').selectmenu('refresh', true);
         $('#model').selectmenu();
         $('#model').selectmenu('refresh', true);
         $('#elevation').selectmenu();
         $('#elevation').selectmenu('refresh', true);
		} else {
			adminBox.html("");
		}
	}
}



// Used to validate the current user password and set 
// the 'auth' variable to true.
function confirm_password(nextNumber) {
	var password = stage.getVariable("password");
	var currentPassword = stage.getVariable("currentPassword");

	// Add the number to the current password and hilight
	if (currentPassword.length !== 4) {
		currentPassword = currentPassword + nextNumber;
		lotStage.getSymbol('auth').$('key' + nextNumber).css({"background-color":"#8b302b"});
	}

	stage.setVariable("currentPassword", currentPassword);

	if (currentPassword === password) {
		stage.setVariable("auth", true);
		lotStage.getSymbol("auth").play("keys-out");
		lotStage.play("fade-out-controls");
	} else if (currentPassword.length === 4) {
		stage.setVariable("currentPassword", "");
		lotStage.getSymbol("auth").$("keyBox").children().css({"background-color":"#b5121b"});
	}
}
window.confirm_password = confirm_password;

function get_polygon_centroid(pts){
   var twicearea=0,
       x=0, y=0,
       nPts = pts.length,
       p1, p2, f;
   for (var i=0, j=nPts-1 ;i<nPts;j=i++) {
      p1=pts[i]; p2=pts[j];
      twicearea+=parseInt(p1.x, 10)*parseInt(p2.y, 10);
      twicearea-=parseInt(p1.y, 10)*parseInt(p2.x, 10);
      f=parseInt(p1.x, 10)*parseInt(p2.y, 10)-parseInt(p2.x, 10)*parseInt(p1.y, 10);
      x+=(parseInt(p1.x, 10)+parseInt(p2.x, 10))*f;
      y+=(parseInt(p1.y, 10)+parseInt(p2.y, 10))*f;
   }
   f=twicearea*3;
   return {x: x/f,y:y/f};
}

/**
 * Returns a random integer between min and max
 * Using Math.round() will give you a non-uniform distribution!
 */
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}


function init() {
	if (window.process !== undefined) {
		var fs = require('fs');
		fs.readFile('models.json', selectionReady);
	} else {
		$.getJSON('models.json', selectionReady);
	}

	var kineticStage = new Kinetic.Stage({
		container: 'Stage_lotMap_lotMap',
		width: 1920,
		height: 979
	});

	var layer = new Kinetic.Layer();

	var mapGroup = new Kinetic.Group({
	 x: -500,
	 y: -1250,
	 draggable: true
	});

	var theaterBackground = new Kinetic.Rect({
		x: -4000,
		y: -3000,
		width: 10000,
		height: 8000,
		fill: 'black',
      opacity: 0
	});
	theaterBackground.isTheater = true;

   /* mEdit code not used
	function lotTheaterMode() {
		var children = mapGroup.getChildren();
		var allInvisible = true;

		// Skip over the background lot map image and the theater rect
		for (var i = 2; i < children.length; i++) {
			var child = children[i];

			// Status is always visible, ignore the status circles
			if (child.getOpacity() !== 0.4 && child.className !== 'Circle') {
				allInvisible = false;
				break;
			}
		}

		if (allInvisible) {
			theaterBackground.setOpacity(0);
		} else {
			theaterBackground.setOpacity(0);
		}

		layer.draw();
	}
   */

	function clearLots() {
		var children = mapGroup.getChildren();

		// Skip over the background lot map image
		for (var i = 1; i < children.length; i++) {
			var child = children[i];

			// Skip the status circles
			if (child.flyout) {
				child.destroy();
				i--;
			} else if (child.isStatic !== true) {
				if (child.isTheater) {
					child.setOpacity(0);
				} else {
					child.setOpacity(0.2);
				}
			}
		}

		mapGroup.setScale(0.3);
		mapGroup.setX(200);
		mapGroup.setY(0);
		mapGroup.setDraggable(true);

		stage.setVariable("zoomed", 30);
		stage.setVariable("currentZoom", 30);

		layer.draw();
	}
	window.clearLots = clearLots;

	function zoom(zoomLevel, up) {
		if (!window.animating) {
         var changed = false;

   		// Adjust the zoomLevel based on if
   		// it is zooming in or out
   		if (up) {
   			if (zoomLevel !== 20) {
   				zoomLevel -= 10;
   				changed = true;
   			}
   		} else {
   			if (zoomLevel !== 100) {
   				zoomLevel += 10;
   				changed = true;
   			}
   		}

   		if (changed) {
   			// Set new bounds on the canvas
   			switch (zoomLevel) {
   				case 20:
   					//mapGroup.setX(225);
   					//mapGroup.setY(-50);
   					//mapGroup.setDraggable(false);
   					break;
   				case 40:
   					//mapGroup.setDraggable(true);
   					break;
   				case 60:
   					//mapGroup.setDraggable(true);
   					break;
   				case 80:
   					//mapGroup.setDraggable(true);
   					break;
   				default:
   					//mapGroup.setX(-500);
   					//mapGroup.setY(-1250);
   					//mapGroup.setDraggable(true);
   					break;
   			}

   			stage.setVariable("zoomed", zoomLevel);

   			var percentZoom = zoomLevel / 100;

   			var currentPos = mapGroup.getAbsolutePosition();
   			console.log(currentPos);

   			if (up) {
               var testX = currentPos.x + 280;
               var testY = currentPos.y + 330;
            } else {
               var testX = currentPos.x - 280;
               var testY = currentPos.y - 330;
            }     		  

   			var zoomTweenMain = new Kinetic.Tween({
   			  node: mapGroup, 
   			  duration: 1,
   			  x: testX, //currentPos.x * percentZoom, // - (currentPos.x * percentZoom),
   			  y: testY, //currentPos.y * percentZoom, // - (currentPos.y / percentZoom),
   			  //offsetX: currentPos.x * 0.2,
   			  //offsetY: currentPos.y * 0.2,
   			  scaleX: percentZoom,
   			  scaleY: percentZoom
   			});

   			var children = mapGroup.getChildren();

            // Skip over the background lot map image
            for (var i = 1; i < children.length; i++) {
               var child = children[i];

               // Skip the status circles
               if (child.flyout) {
                  var zoomTweenFlyout = new Kinetic.Tween({
                    node: child, 
                    duration: 1,
                    scaleX: 1 + ((1 - percentZoom) * 3),
                    scaleY: 1 + ((1 - percentZoom) * 3)
                  });

                  zoomTweenFlyout.play()
               }
            }

            zoomTweenMain.play();
            window.animating = true;

            setTimeout(function (){
               window.animating = false;
            }, 1000);
         }
		}
	}
	window.zoom = zoom;

	function createFlyout(polygon, circle) {
		var children = mapGroup.getChildren();
		var count = 0;
		var flyoutStartIndex;
		// Skip over the background lot map image
		for (var i = 1; i < children.length; i++) {
			var child = children[i];

			if (child.flyout) {
				// remember where the oldest flyout is
				if (flyoutStartIndex === undefined) {
					flyoutStartIndex = i;
				}
				count++;
				if (count >= 5) {
					// unhighlight the parent and remove the flyout
					children[children[flyoutStartIndex].lot].setOpacity(0.2);
					children[flyoutStartIndex].destroy();
					i--;
				}
			}
		}

		var flyoutGroup = new Kinetic.Group({
         x: circle.getX(),
         y: circle.getY(),
         draggable: true
      });
		flyoutGroup.lot = polygon.index;
		flyoutGroup.flyout = true;
		window.currentLot = polygon.index;

		var flyoutBox = new Kinetic.Rect({
			width: 250,
			height: 150,
			cornerRadius: 25,
			opacity: 1,
			fill: '#b7a9a4',
			shadowColor: '#333',
			shadowOffsetY: 3,
			shadowOffsetX: 3
		});

		var lotStatus = new Kinetic.Text({
	        x: 0,
	        y: -5,
	        text: 'This lot is ' + polygon.data.status,
	        fontSize: 20,
	        fontFamily: 'Calibri',
	        fill: polygon.data.statusColour,
	        width: 250,
	        padding: 20
	      });

		var lotName = new Kinetic.Text({
	        x: 0,
	        y: lotStatus.getY() + 35,
	        text: 'Lot: ' + polygon.data.lot,
	        fontSize: 20,
	        fontFamily: 'Trade Gothic',
	        fill: 'black',
	        width: 250,
	        padding: 20
	      });

      var lotPrice = new Kinetic.Text({
        x: 0,
        y: lotName.getY() + 35,
        text: 'Lot Price: $' + polygon.data.price,
        fontSize: 20,
        fontFamily: 'Trade Gothic',
        fill: 'black',
        width: 250,
        padding: 20
      });

      var lotCloseBox = new Kinetic.Rect({
         x: 35,
         y: lotPrice.getY() + 55,
         opacity: 0.5,
         fill: 'black',
         cornerRadius: 10,
         width: 78,
         height: 25
      });


      var lotClose = new Kinetic.Text({
         x: 22,
         y: lotPrice.getY() + 40,
         text: 'CLOSE BOX',
         fontSize: 16,
         fontFamily: 'Trade Gothic',
         fontStyle: 'normal',
         fill: 'white',
         width: 250,
         padding: 20
      });


      var modelName =  polygon.data.model.replace(/ /g,'');
      modelName = modelName.charAt(0).toLowerCase() + modelName.slice(1);

      var elevationName = polygon.data.elevation.toLowerCase().replace(/[^\w-]+/g,'').replace(/ /g,'-');

      var modelImage = new Image();
      
      console.log("Model Name: " + modelName);
      console.log("Elevation Name: " + elevationName);

      if (modelName === "" | elevationName === "") { 
         modelImage.src = "";
      } else {
         modelImage.src = "images/" + modelName + "/" + elevationName + ".jpg";
         //modelImage.src = "images/happyHouse.jpeg";
      }

      function flyoutCloseClick(event) {
         console.log("flyout close click");
         polygon.setOpacity(0.2);

         var children = mapGroup.getChildren();
         for (var i = 0; i < children.length; i++) {
            var child = children[i];

            if (child.lot === polygon.index) {
            	child.destroy();
            	/* //mEdit refresh when closing flyout
            	var auth = stage.getVariable("auth");
            	if (auth){
            		stage.getSymbol('lotMap').getSymbol('auth').$('adminBox').html("");
            		window.changePhase();
            	} */
            	break;
            }
         }
         layer.draw();
      };

      modelImage.onload = function() {
			var lotImage = new Kinetic.Image({
				 x: 225,
				 y: 25,
				 stroke: 'black',
				 strokeWidth: 2,
				 image: modelImage,
				 width: 100,
				 height: 100
			 });

         function modelLotClick(event) {
            //console.log(stage);
            //console.log(polygon.data);

            stage.setVariable("auth", false);
            stage.getSymbol('lotMap').getSymbol('auth').$('adminBox').html("");

            stage.setVariable("currentScreen", "brochure");

				var data = stage.getVariable("rawData");

				for (var i = 0; i < data.length; i++) {
					var modelData = data[i];

					if (modelData.name === polygon.data.model) {
						//window.clearLots(); //mEdit
						stage.setVariable("currentModel", modelData);
						stage.play("fade-out-selection");
						break;
					}
				}
         }

			if(!!('ontouchstart' in window)){
				lotImage.on('touchend', modelLotClick);
			} else {
				lotImage.on('click', modelLotClick);
			}

         if(!!('ontouchstart' in window)){
            lotClose.on('touchend', flyoutCloseClick);
         } else {
            lotClose.on('click', flyoutCloseClick);
         }

			flyoutBox.setWidth(flyoutBox.getWidth() + lotImage.getWidth());

   		var lotModel = new Kinetic.Text({
           x: lotImage.getX(),
           y: lotImage.getY(),
           text: polygon.data.model,
           fontSize: 16,
           fontStyle: 'bold',
           fontFamily: 'Calibri',
           fill: 'black'
         });

         lotModel.setX(lotImage.getX() + ((lotImage.getWidth()/2) - (lotModel.getWidth()/2)));
         lotModel.setY(lotImage.getY() - lotModel.getHeight() - 5);

         var lotElevation = new Kinetic.Text({
           x: lotImage.getX(),
           y: lotImage.getY(),
           text: polygon.data.elevation,
           fontSize: 16,
           fontStyle: 'bold',
           fontFamily: 'Calibri',
           fill: 'black'
         });

         lotElevation.setX(lotImage.getX() + ((lotImage.getWidth()/2) - (lotElevation.getWidth()/2)));
         lotElevation.setY(lotImage.getY() + lotImage.getHeight() + lotElevation.getHeight() - 10);

         if (polygon.data.status === 'Available') {
            flyoutBox.setWidth(flyoutBox.getWidth() - lotImage.getWidth());
            flyoutGroup.add(flyoutBox).add(lotStatus).add(lotName).add(lotPrice).add(lotCloseBox).add(lotClose);
            mapGroup.add(flyoutGroup);
         } else {
            flyoutGroup.add(flyoutBox).add(lotStatus).add(lotName).add(lotPrice).add(lotImage).add(lotModel).add(lotElevation).add(lotCloseBox).add(lotClose);
            mapGroup.add(flyoutGroup);
         }

         var percentZoom = stage.getVariable("zoomed") / 100;

         flyoutGroup.setScale(1 + ((1 - percentZoom) * 3));

   		layer.draw();
      };

		modelImage.onerror = function() {

         if(!!('ontouchstart' in window)){
            lotClose.on('touchend', flyoutCloseClick);
         } else {
            lotClose.on('click', flyoutCloseClick);
         }

			flyoutGroup.add(flyoutBox).add(lotStatus).add(lotName).add(lotPrice).add(lotCloseBox).add(lotClose);
			mapGroup.add(flyoutGroup);

         var percentZoom = stage.getVariable("zoomed") / 100;

         flyoutGroup.setScale(1 + ((1 - percentZoom) * 3));

			layer.draw();
		}
	};

	function lotUpdate(select) {
		console.log('A lot was updated!');

		var children = mapGroup.getChildren();
		var lot = children[window.currentLot];
		var select = $(select);

      var attribute = select.attr('id');
      //var value = $('#' + attribute + 'option:selected').text();
      var value = select.val();

		console.log('It was this one: ' + JSON.stringify(lot));
		console.log("SET [ " + attribute + " ] TO [ " + value + " ]");

		

		lot.data[attribute] = value;

		if (attribute === 'status') {
			for (var status in STATUSES) {
				if (STATUSES[status].status === value) {
					lot.data.statusColour = STATUSES[status].colour;
					children[window.currentLot].setFill(lot.data.statusColour);
					break;
				}
			}
		}

		if (window.process != undefined) {
			var fs = require('fs')
				, data = JSON.parse(fs.readFileSync(window.dataPath + 'lots.json'))
				, dataChanged = false
				;

			for (i = 0; i < data.length; i++) {
				if (data[i].lot === lot.data.lot && data[i].block === lot.data.block) {
					data[i][attribute] = value;
					dataChanged = true;
					break;
				}
			}

			if (dataChanged) {
				console.log('Writing to the file');
				fs.writeFileSync(window.dataPath + 'lots.json', JSON.stringify(data));
			}
		} else {
			console.log('CAN\'T WRITE TO FILE. Not being run by node-webkit. TODO: Use localStorage instead.');
		}

		for (var i = 0; i < children.length; i++) {
			var child = children[i];

			if (child.lot === window.currentLot) {
				child.destroy();
				console.log("destroy and recreate flyout " + window.currentLot);
				createFlyout(children[window.currentLot], children[window.currentLot + 1]);
				break;
			}
		}

		layer.draw();
	}
	window.lotUpdate = lotUpdate;


	var phase = stage.getVariable("currentPhase");

	var lotMap = new Image();
	lotMap.src = "images/mahogany-phase" + phase + ".png";
	var miniMap = new Image();
	miniMap.src = "images/mhat-comm-" + phase + "-thumb.png";
	var mapLegend = new Image();
	mapLegend.src = "images/map-legend.png";

	lotMap.onload = function() {
		var kineticImage = new Kinetic.Image({
			x: 0,
			y: 0,
			image: lotMap,
			width: lotMap.width,
			height: lotMap.height
		});

		function dataLoaded(err, data) {
			if (data !== 'success') {
				data = JSON.parse(data);
			} else {
				data = err;
			}

			//miniMap.onload = function() {
				var miniMapRect = new Kinetic.Rect({
					x: lotMap.width + 50,
					y: 50,
					width: 737,
					height: 800,
					stroke: 'black',
					strokeWidth: 3,
					fill: 'white',
					opacity: 100
				});
				miniMapRect.isStatic = true;
				var miniMapImage = new Kinetic.Image({
					x: lotMap.width + 100,
					y: 100,
					image: miniMap,
					width: 637,
					height: 550
				});
				miniMapImage.isStatic = true;
				var miniMapText = new Kinetic.Text({
					x: lotMap.width + 100,
					y: 700,
					text: 'VIEW COMMUNITY',
					fontSize: 100,
					fontFamily: 'Trade Gothic',
					fill: 'black',
					align: 'center',
					width: 637
				});
				miniMapText.isStatic = true;
				var mapLegendImage = new Kinetic.Image({
					x: lotMap.width + 100,
					y: 900,
					image: mapLegend,
					width: 550,
					height: 610
				});
				mapLegendImage.isStatic = true;
				function minimapClick(event) {
					console.log("minimap clicked");
					lotStage.$('miniMap').css({"background-image":"url(images/mhat-comm-" + phase + ".jpg)"});
					lotStage.play("fade-in-miniMap");
				};
				if(!!('ontouchstart' in window)){
					miniMapRect.on('touchend', minimapClick);
					miniMapImage.on('touchend', minimapClick);
					miniMapText.on('touchend', minimapClick);
				} else {
					miniMapRect.on('click', minimapClick);
					miniMapImage.on('click', minimapClick);
					miniMapText.on('click', minimapClick);
				}
			//}

			mapGroup.add(kineticImage).add(theaterBackground).add(miniMapRect).add(miniMapImage).add(miniMapText).add(mapLegendImage);

			for (var i = 0; i < data.length; i++) {
				if (data[i].phase === phase) {
					var polygon = new Kinetic.Polygon({
					  points: data[i].points,
					  opacity: 0.2,
					  fill: '#FFFFFFF',
					  stroke: 'black',
					  strokeWidth: 2
					});

					polygon.data = data[i];

					var colour = 'black';
					for (var status in STATUSES) {
						status = STATUSES[status];
						if (polygon.data.status === status.status) {
							colour = status.colour;
							break;
						} 
					}

					polygon.data.statusColour = colour;

					polygon.setFill(polygon.data.statusColour);

					function polygonClick(event) {
					  var children = mapGroup.getChildren();

					  if (this.getOpacity() === 0.2) {
					  	console.log("click to create flyout");
						this.setOpacity(0.4);
						createFlyout(this, children[this.index + 1]);
						create_auth_form(this.data);
					  } else {
						this.setOpacity(0.2);

						for (var i = 0; i < children.length; i++) {
							var child = children[i];

							if (child.lot === this.index) {
								child.destroy();
								/* //mEdit refresh when closing flyout
								var auth = stage.getVariable("auth");
								if (auth){
									stage.getSymbol('lotMap').getSymbol('auth').$('adminBox').html("");
									window.changePhase();
								} */
								break;
							}
						}
                  layer.draw();
						create_auth_form();	
					  }
						//lotTheaterMode();
					}

               if(!!('ontouchstart' in window)){
                  polygon.on('touchend', polygonClick);
               } else {
                  polygon.on('click', polygonClick);
               }

					var center = get_polygon_centroid(polygon.data.points);

					var circle = new Kinetic.Circle({
					  x: center.x,
					  y: center.y
					});

					/* mEdit Is this code necessary?
					// dEdit No but without the circles a whole lot of the other code doesn't
					// work as expected.
					if(!!('ontouchstart' in window)){
						circle.on('touchend', circleClick);
					} else {
						circle.on('click', circleClick);
					}

					function circleClick(event) {
						// mapGroup = [MapImage, TheaterRect, Polygon1, Circle1, Polygon2, Circle2, ...]
						if(!!('ontouchstart' in window)){
							mapGroup.getChildren()[this.index - 1].fire('touchend');
						} else {
							mapGroup.getChildren()[this.index - 1].fire('click');
						}
					} */

					mapGroup.add(polygon).add(circle);
				}
			};

			mapGroup.setScale(0.3);
			mapGroup.setX(200);
			mapGroup.setY(0);
			mapGroup.setDraggable(true);

			stage.setVariable("zoomed", 30);
			stage.setVariable("currentZoom", 30);

			layer.add(mapGroup);
			kineticStage.add(layer);

      function changePhase() {
         function dataReloaded(err, data) {

         if (data !== 'success') {
   			data = JSON.parse(data);
   		} else {
   			data = err;
   		}
         //console.log('My current Children: ' + layer.children.length)

         //mapGroup.destroyChildren();
         layer.destroyChildren();

         //console.log('I\'ve killed them all: ' + layer.children.length)

         var theaterBackground = new Kinetic.Rect({
            x: 0,
            y: 0,
            width: 5000,
            height: 4000,
            opacity: 0,
            fill: 'red'
         });
         theaterBackground.isTheater = true;

         var phase = stage.getVariable("currentPhase");

		var miniMap = new Image();
		miniMap.src = "images/mhat-comm-" + phase + "-thumb.png";
		var lotMap = new Image();
		lotMap.src = "images/mahogany-phase" + phase + ".png";
		var mapLegend = new Image();
		mapLegend.src = "images/map-legend.png";

         lotMap.onload = function() {
            var kineticImage = new Kinetic.Image({
                x: 0,
                y: 0,
                image: lotMap,
                width: lotMap.width,
                height: lotMap.height,
                draggable: true
             });

			//miniMap.onload = function() {
				var miniMapRect = new Kinetic.Rect({
					x: lotMap.width + 50,
					y: 50,
					width: 737,
					height: 800,
					stroke: 'black',
					strokeWidth: 3,
					fill: 'white',
					opacity: 100
				});
				miniMapRect.isStatic = true;
				var miniMapImage = new Kinetic.Image({
					x: lotMap.width + 100,
					y: 100,
					image: miniMap,
					width: 637,
					height: 550
				});
				miniMapImage.isStatic = true;
				var miniMapText = new Kinetic.Text({
					x: lotMap.width + 100,
					y: 700,
					text: 'VIEW COMMUNITY',
					fontSize: 100,
					fontFamily: 'Trade Gothic',
					fill: 'black',
					align: 'center',
					width: 637
				});
				miniMapText.isStatic = true;
				var mapLegendImage = new Kinetic.Image({
					x: lotMap.width + 100,
					y: 900,
					image: mapLegend,
					width: 550,
					height: 610
				});
				mapLegendImage.isStatic = true;
				function minimapClick(event) {
					console.log("minimap clicked");
					lotStage.$('miniMap').css({"background-image":"url(images/mhat-comm-" + phase + ".jpg)"});
					lotStage.play("fade-in-miniMap");
				};
				if(!!('ontouchstart' in window)){
					miniMapRect.on('touchend', minimapClick);
					miniMapImage.on('touchend', minimapClick);
					miniMapText.on('touchend', minimapClick);
				} else {
					miniMapRect.on('click', minimapClick);
					miniMapImage.on('click', minimapClick);
					miniMapText.on('click', minimapClick);
				}
			//}

   			mapGroup.add(kineticImage).add(theaterBackground).add(miniMapRect).add(miniMapImage).add(miniMapText).add(mapLegendImage);


            for (var i = 0; i < data.length; i++) {
               if (data[i].phase === phase) {
                  var polygon = new Kinetic.Polygon({
                    points: data[i].points,
                    opacity: 0.2,
                    fill: '#FFFFFFF',
                    stroke: 'black',
                    strokeWidth: 2
                  });

                  polygon.data = data[i];

                  var colour = 'black';
                  for (var status in STATUSES) {
                     status = STATUSES[status];
                     if (polygon.data.status === status.status) {
                        colour = status.colour;
                        break;
                     } 
                  }

                  polygon.data.statusColour = colour;

                  polygon.setFill(polygon.data.statusColour);



                  if(!!('ontouchstart' in window)){
                     polygon.on('touchend', polygonClick);
                  } else {
                     polygon.on('click', polygonClick);
                  }


                  var center = get_polygon_centroid(polygon.data.points);

                  var circle = new Kinetic.Circle({
                    x: center.x,
                    y: center.y,
                  });

                  /* mEdit
                  if(!!('ontouchstart' in window)){
                     circle.on('touchend', circleClick);
                  } else {
                     circle.on('click', circleClick);
                  }
                  */

                  mapGroup.add(polygon).add(circle);
               }
            }

               mapGroup.setScale(0.3);
               mapGroup.setX(200);
               mapGroup.setY(0);
               mapGroup.setDraggable(true);

               stage.setVariable("zoomed", 30);
               stage.setVariable("currentZoom", 30);

               layer.clear();
               layer.add(mapGroup);
               layer.draw();
            }
         }

         if (window.process !== undefined) {
   			var fs = require('fs');
   			fs.readFile(window.dataPath + 'lots.json', dataReloaded);
   		} else {
   			$.getJSON('lots.json', dataReloaded);
   		}

      }
      window.changePhase = changePhase;

   		};


   		if (window.process !== undefined) {
   			var fs = require('fs');
   			fs.readFile(window.dataPath + 'lots.json', dataLoaded);
   		} else {
   			$.getJSON('lots.json', dataLoaded);
   		}
   	}
}


window.init = init;

      });
      //Edge binding end

      

      

      

      

      

      

      

      

      

      

      Symbol.bindElementAction(compId, symbolName, "${_close}", "click", function(sym, e) {
         var stage = sym.getComposition().getStage();
         stage.setVariable("currentScreen", "selection");
         
         sym.play('fade-out-web');

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 250, function(sym, e) {
         sym.stop();
         
         var stage = sym.getComposition().getStage();
         
         var viewWeb = sym.$("webContent");
         var site = stage.getVariable("currentSite");
         
         viewWeb.html('<div id="wrap" style="width: 1920px; height: 1080px; padding: 0; overflow: hidden;"> <iframe width="1920px" height="1080px" src="' + site + '" style="border-style:none;"> </div>');

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 750, function(sym, e) {
         var stage = sym.getComposition().getStage();
         var screen = stage.getVariable("currentScreen");
         
         stage.$("webContainer").hide();
         sym.stop();
         
         stage.play('fade-in-' + screen);
         
         var viewWeb = sym.$("webContent");
         viewWeb.html('');

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 2250, function(sym, e) {
         var stage = sym.getComposition().getStage();
         var lotStage = stage.getSymbol('lotMap');
         
         sym.stop();
         
         var currentPhase = stage.getVariable("currentPhase");
         lotStage.$('text' + currentPhase).css({'color':'white'});
         lotStage.play('fade-in-lotMap');

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 2750, function(sym, e) {
         var stage = sym.getComposition().getStage();
         var screen = stage.getVariable("currentScreen");
         
         stage.$("lotMap").hide();
         sym.stop();
         
         stage.play('fade-in-' + screen);

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 3250, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 3750, function(sym, e) {
         var stage = sym.getComposition().getStage();
         var screen = stage.getVariable("currentScreen");
         
         stage.$("brochure").hide();
         window.clearBrochure();
         sym.stop();
         
         stage.play('fade-in-' + screen);

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 2000, function(sym, e) {
         var stage = sym.getComposition().getStage();
         
         stage.$("lotMap").show();
         
         var keys = stage.getVariable("keys");
         
         if (keys) {
         	sym.getSymbol("auth").play("keys-out");
         }
         
         stage.setVariable("auth", false);
         stage.getSymbol('lotMap').getSymbol('auth').$('adminBox').html("");

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 1250, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 1750, function(sym, e) {
         var stage = sym.getComposition().getStage();
         var screen = stage.getVariable("currentScreen");
         
         stage.$("modelSelection").hide();
         
         sym.stop();
         
         console.log('!!! fade-in-' + screen + ' !!!');
         stage.play('fade-in-' + screen);

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 1000, function(sym, e) {
         var stage = sym.getComposition().getStage();
         
         stage.$("modelSelection").show();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 3000, function(sym, e) {
         var stage = sym.getComposition().getStage();
         
         stage.setVariable("currentOptions", {});
         window.loadBrochure(function() { stage.$("brochure").show(); });

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 3875, function(sym, e) {
         var stage = sym.getComposition().getStage();
         
         stage.$("community").show();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 4125, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 4625, function(sym, e) {
         var stage = sym.getComposition().getStage();
         var screen = stage.getVariable("currentScreen");
         
         stage.$("community").hide();
         sym.stop();
         
         stage.play('fade-in-' + screen);

      });
      //Edge binding end

   })("stage");
   //Edge symbol end:'stage'

   //=========================================================
   
   //Edge symbol: 'zoom'
   (function(symbolName) {   
   
      Symbol.bindElementAction(compId, symbolName, "${_zoomRect}", "click", function(sym, e) {
         var stage = sym.getComposition().getStage();
         
         var zoomed = stage.getVariable("zoomed");
         
         window.zoom(zoomed, false);

      });
         //Edge binding end

   })("zoomIn");
   //Edge symbol end:'zoomIn'

   //=========================================================
   
   //Edge symbol: 'auth'
   (function(symbolName) {   
   
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 500, function(sym, e) {
         var stage = sym.getComposition().getStage();
         
         sym.stop();
         
         stage.setVariable("keys", true);
         stage.setVariable("currentPassword", "");

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 1500, function(sym, e) {
         var stage = sym.getComposition().getStage();
         
         sym.stop();
         
         stage.setVariable("keys", false);
         stage.setVariable("currentPassword", "");
         sym.$("keyBox").children().css({"background-color":"#b5121b"});

      });
      //Edge binding end

      

      Symbol.bindElementAction(compId, symbolName, "${_key0}", "click", function(sym, e) {
         window.confirm_password("0");

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_key9}", "click", function(sym, e) {
         window.confirm_password("9");

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_key8}", "click", function(sym, e) {
         window.confirm_password("8");

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_key7}", "click", function(sym, e) {
         window.confirm_password("7");

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_key6}", "click", function(sym, e) {
         window.confirm_password("6");

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_key5}", "click", function(sym, e) {
         window.confirm_password("5");

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_key4}", "click", function(sym, e) {
         window.confirm_password("4");

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_key3}", "click", function(sym, e) {
         window.confirm_password("3");

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_key2}", "click", function(sym, e) {
         window.confirm_password("2");

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_key1}", "click", function(sym, e) {
         window.confirm_password("1");

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_authClick}", "click", function(sym, e) {
         var stage = sym.getComposition().getStage();
         var lotStage = stage.getSymbol('lotMap');
         var keys = stage.getVariable("keys");
         var auth = stage.getVariable("auth");
         
         if (auth) {
         	stage.setVariable("auth", false);
         	lotStage.play("fade-in-controls");
         	sym.$('adminBox').html("");
         	window.changePhase();
         } else {
         	if (keys) {
         		sym.play('keys-out');
         	} else {
         		sym.play('keys-in');
         	}
         	stage.setVariable("keys", !keys);
         }
         

      });
      //Edge binding end

   })("auth");
   //Edge symbol end:'auth'

   //=========================================================
   
   //Edge symbol: 'zoom_1'
   (function(symbolName) {   
   
      Symbol.bindElementAction(compId, symbolName, "${_zoomRect}", "click", function(sym, e) {
         var stage = sym.getComposition().getStage();
         
         var zoomed = stage.getVariable("zoomed");
         
         window.zoom(zoomed, true);

      });
            //Edge binding end

      })("zoomOut");
   //Edge symbol end:'zoomOut'

   //=========================================================
   
   //Edge symbol: 'lotMap'
   (function(symbolName) {   
   
      

      Symbol.bindElementAction(compId, symbolName, "${_clearRect}", "click", function(sym, e) {
         var stage = sym.getComposition().getStage();
         
         var keys = stage.getVariable("keys");
         if (keys) {
         	sym.getSymbol("auth").play("keys-out");
         }
         
         stage.setVariable("auth", false);
         sym.getSymbol('auth').$('adminBox').html("");
         
         window.clearLots();

      });
         //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 250, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 1750, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 2250, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_back}", "click", function(sym, e) {
         var stage = sym.getComposition().getStage();
         
         stage.setVariable("auth", false);
         stage.getSymbol('lotMap').getSymbol('auth').$('adminBox').html("");
         
         stage.setVariable("currentScreen", "selection");
         stage.play("fade-out-lotMap");
         window.changePhase();
         

      });
      //Edge binding end

      

      Symbol.bindElementAction(compId, symbolName, "${_click19}", "click", function(sym, e) {
         var stage = sym.getComposition().getStage();
         
         stage.setVariable("currentPhase", "8");
         
         window.changePhase();
         
         sym.$('text19').css({'color':'white'});
         sym.$('text20').css({'color':'black'});
         sym.$('text22').css({'color':'black'});
         sym.$('text24').css({'color':'black'});
         sym.$('text45').css({'color':'black'});

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_click20}", "click", function(sym, e) {
         var stage = sym.getComposition().getStage();
         
         stage.setVariable("currentPhase", "20");
         
         window.changePhase();
         
         sym.$('text19').css({'color':'black'});
         sym.$('text20').css({'color':'white'});
         sym.$('text22').css({'color':'black'});
         sym.$('text24').css({'color':'black'});
         sym.$('text45').css({'color':'black'});

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_click22}", "click", function(sym, e) {
         var stage = sym.getComposition().getStage();
         
         stage.setVariable("currentPhase", "12");
         
         window.changePhase();
         
         sym.$('text19').css({'color':'black'});
         sym.$('text20').css({'color':'black'});
         sym.$('text22').css({'color':'white'});
         sym.$('text24').css({'color':'black'});
         sym.$('text45').css({'color':'black'});

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_click24}", "click", function(sym, e) {
         var stage = sym.getComposition().getStage();
         
         stage.setVariable("currentPhase", "15");
         
         window.changePhase();
         
         sym.$('text19').css({'color':'black'});
         sym.$('text20').css({'color':'black'});
         sym.$('text22').css({'color':'black'});
         sym.$('text24').css({'color':'white'});
         sym.$('text45').css({'color':'black'});

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 750, function(sym, e) {
         var stage = sym.getComposition().getStage();
         stage.setVariable("auth", false);
         stage.getSymbol('lotMap').getSymbol('auth').$('adminBox').html("");
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 1250, function(sym, e) {
         sym.play(250);

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_miniMapClose}", "click", function(sym, e) {
         sym.play("fade-out-miniMap");

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_click45}", "click", function(sym, e) {
         var stage = sym.getComposition().getStage();
         
         stage.setVariable("currentPhase", "45");
         
         window.changePhase();
         
         sym.$('text19').css({'color':'black'});
         sym.$('text20').css({'color':'black'});
         sym.$('text22').css({'color':'black'});
         sym.$('text24').css({'color':'black'});
         sym.$('text45').css({'color':'white'});

      });
      //Edge binding end

   })("lotMap");
   //Edge symbol end:'lotMap'

   //=========================================================
   
   //Edge symbol: 'modelSelection'
   (function(symbolName) {   
   
      

      

      

      Symbol.bindElementAction(compId, symbolName, "${_lotLink}", "click", function(sym, e) {
         var stage = sym.getComposition().getStage();
         
         stage.setVariable("currentScreen", "lotMap");
         stage.play("fade-out-selection");
         

      });
      //Edge binding end

      

      Symbol.bindElementAction(compId, symbolName, "${_stepperLink}", "click", function(sym, e) {
         var stage = sym.getComposition().getStage();
         
         stage.setVariable("currentScreen", "web");
         stage.setVariable("currentSite", "http://stepperhomes.com/");
         stage.play("fade-out-selection");

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_homeAlbumLink}", "click", function(sym, e) {
         var stage = sym.getComposition().getStage();
         
         stage.setVariable("currentScreen", "web");
         stage.setVariable("currentSite", "http://www.stepperhomes.com/the-home-album.html");
         stage.play("fade-out-selection");

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_communityLink}", "click", function(sym, e) {
         var stage = sym.getComposition().getStage();
         
         stage.setVariable("currentScreen", "community");
         stage.play("fade-out-selection");
         

      });
      //Edge binding end

   })("modelSelection");
   //Edge symbol end:'modelSelection'

   //=========================================================
   
   //Edge symbol: 'brochure'
   (function(symbolName) {   
   
      Symbol.bindElementAction(compId, symbolName, "${_back}", "click", function(sym, e) {
         var stage = sym.getComposition().getStage();
         
         stage.setVariable("currentScreen", "selection");
         sym.play("fade-out-email");
         stage.play("fade-out-brochure");
         
         

      });
      //Edge binding end

      

      

      

      

      

      

      

      

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 250, function(sym, e) {
         var stage = sym.getComposition().getStage();
         
         
         var rawData = stage.getVariable("rawData");
         var currentOptions = stage.getVariable("currentOptions");
         
         
         var emailKeyboard = {
         	display: {
         		'bksp'   : '\u2190',
         		'enter'  : 'return',
         		'default': 'ABC',
         		'meta1'  : '.?123',
         		'meta2'  : '#+=',
         		'accept' : 'ENTER'
         	},
         
         	layout: 'custom',
         
         	customLayout: {
         
         		'default': [
         			'q w e r t y u i o p {bksp}',
         			'a s d f g h j k l _ -',
         			'{s} z x c v b n m @ . {s}',
         			'{meta1} {space} {accept}'
         		],
         		'shift': [
         			'Q W E R T Y U I O P {bksp}',
         			'A S D F G H J K L _ -',
         			'{s} Z X C V B N M @ . {s}',
         			'{meta1} {space} {accept}'
         		],
         		'meta1': [
         			'1 2 3 4 5 6 7 8 9 0 {bksp}',
         			'` | { } % ^ * / \' {enter}',
         			'{meta2} $ & ~ # = + . {meta2}',
         			'{default} {space} ! ? {accept}'
         		],
         		'meta2': [
         			'[ ] { } \u2039 \u203a ^ * " , {bksp}',
         			'\\ | / < > $ \u00a3 \u00a5 \u2022 {enter}',
         			'{meta1} \u20ac & ~ # = + . {meta1}',
         			'{default} {space} ! ? {accept}'
         		]
         
         	}
         }
         
         
         var form = sym.$("emailContainer").find("div[id$='emailForm']");
         
         
         $(form).html('<div class="brochureForm"> \
         	<div class="form-group"> \
         	  <label for="first">First Name</label> \
         	  <input type="text" class="form-control" id="first" name="first"> \
         	</div> \
         	<div class="form-group"> \
         	  <label for="last">Last Name</label> \
         	  <input type="text" class="form-control" id="last" name="last"> \
         	</div> \
         	<div class="form-group"> \
         	  <label for="email">Email</label> \
         	  <input type="email" class="form-control" id="email" name="email"> \
         	</div> \
         </div>')
         
         $('#first').keyboard(emailKeyboard);
         $('#last').keyboard(emailKeyboard);
         $('#email').keyboard(emailKeyboard);
         
         
         var modelBox = sym.$("emailContainer").find("div[id$='modelBox']");
         var modelsHTML = "";
         
         for (var i = 0; i < rawData.length; i++) {
         	var model = rawData[i];
         	var modelName =  model.name.replace(/ /g,'');
            modelName = modelName.charAt(0).toLowerCase() + modelName.slice(1);
         	var modelUrl = "images/" + modelName + "/select.jpg";
         
         	var modelHTML = "<div class='model-email-box'><img class='model-image' src=" + modelUrl + " /><div id='" + modelName + "' class='model' onclick='window.showHideModel(" + modelName + ")'>" + model.name + "</div><span class='area'>" + model.area + "</span></div>";
         
         	modelsHTML += modelHTML;
         }
         
         modelBox.html(modelsHTML);
         
         sym.$("emailContainer").show();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 600, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 1000, function(sym, e) {
         sym.stop();
         
         sym.$("emailContainer").hide();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_emailLink}", "click", function(sym, e) {
         var emailContainer = sym.$('emailContainer');
         
         if ($(emailContainer).is(":visible")) {
         	sym.play("fade-out-email");
         } else {
         	sym.play("fade-in-email");
         }

      });
      //Edge binding end

      

      

      

      

      

      Symbol.bindElementAction(compId, symbolName, "${_galleryLink}", "click", function(sym, e) {
         var galleryContainer = sym.$('galleryContainer');
         
         if ($(galleryContainer).is(":visible")) {
         	sym.play("fade-out-gallery");
         } else {
         	sym.play("fade-in-gallery");
         }

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 1500, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 2000, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

   })("brochure");
   //Edge symbol end:'brochure'

   //=========================================================
   
   //Edge symbol: 'modelBox'
   (function(symbolName) {   
   
      

   })("modelBox");
   //Edge symbol end:'modelBox'

   //=========================================================
   
   //Edge symbol: 'community'
   (function(symbolName) {   
   
      Symbol.bindElementAction(compId, symbolName, "${_backLink}", "click", function(sym, e) {
         var stage = sym.getComposition().getStage();
         
         stage.setVariable("currentScreen", "selection");
         sym.play("fade-out-community");
         stage.play("fade-out-community");

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 250, function(sym, e) {
         var stage = sym.getComposition().getStage();
         
         sym.$("communityCanvas").show();
         sym.$("communityCanvas").css({
         	"background-image":"url('images/MHAT-COMMUNITY.jpg')",
         	"background-repeat":"no-repeat",
         	"background-position":"center center"
         });

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 500, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 1000, function(sym, e) {
         sym.$("communityCanvas").hide();
         

      });
      //Edge binding end

      

      Symbol.bindElementAction(compId, symbolName, "${_communityMapLink}", "click", function(sym, e) {
         sym.play("fade-in-community");
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_communityVideo}", "click", function(sym, e) {
         var viewVid = sym.$('videoPlayer');
         viewVid.show();
         
         var myVideo = document.getElementById("videoplay");
         myVideo.currentTime = 0;
         myVideo.play();
         
         myVideo.addEventListener('ended', function() {
         	myVideo.pause();
         	viewVid.hide();
         });

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_videoPlayer}", "click", function(sym, e) {
         var myVideo = document.getElementById("videoplay");
         myVideo.pause();
         
         var viewVid = sym.$('videoPlayer');
         viewVid.hide();

      });
      //Edge binding end

      

      

      Symbol.bindElementAction(compId, symbolName, "${_communityMapClose}", "click", function(sym, e) {
         sym.play("fade-out-community");
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_selectCommunityMap}", "click", function(sym, e) {
         sym.$("communityCanvas").css({
         	"background-image":"url('images/MHAT-COMMUNITY.jpg')",
         	"background-repeat":"no-repeat",
         	"background-position":"center center"
         });

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_selectCentralBeach}", "click", function(sym, e) {
         sym.$("communityCanvas").css({
         	"background-image":"url('images/MHAT-centralbeach.jpg')",
         	"background-repeat":"no-repeat",
         	"background-position":"center center"
         });

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_selectNorthLake}", "click", function(sym, e) {
         sym.$("communityCanvas").css({
         	"background-image":"url('images/MHAT-northlake.jpg')",
         	"background-repeat":"no-repeat",
         	"background-position":"center center"
         });

      });
      //Edge binding end

   })("community");
   //Edge symbol end:'community'

   //=========================================================
   
   //Edge symbol: 'emailContainer'
   (function(symbolName) {   
   
      Symbol.bindElementAction(compId, symbolName, "${_sendBox}", "click", function(sym, e) {
         window.sendEmail();

      });
      //Edge binding end

   })("emailContainer");
   //Edge symbol end:'emailContainer'

   //=========================================================
   
   //Edge symbol: 'galleryContainer'
   (function(symbolName) {   
   
      Symbol.bindElementAction(compId, symbolName, "${_galleryClose}", "click", function(sym, e) {
         sym.getParentSymbol().play("fade-out-gallery");
         

      });
      //Edge binding end

   })("galleryContainer");
   //Edge symbol end:'galleryContainer'

})(jQuery, AdobeEdge, "EDGE-MAHOGANYAT");